Feature: Stock
  Scenario: Achat Fournisseur
    Given un stock de 6 pommes
    When je commande 3 pommes
    Then j'ai un stock de 9 pommes
  Scenario: Vente Client
    Given un stock de 4 pommes
    When je vends 2 pommes
    Then j'ai un stock de 2 pommes
  Scenario: Je vends trop de pommes
    Given un stock de 6 pommes
    When je vends 8 pommes
    Then Aie aie aie